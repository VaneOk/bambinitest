<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Success') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="border rounded-lg md:rounded-r-none text-center p-5 mx-auto md:mx-0 my-2 md:my-6 bg-gray-100 font-medium z-10 shadow-lg">
                    <div class="">Success</div>
                    <div id="annual" class="font-bold text-6xl annual">Order №{{$order_id}}</div>
                    <hr>
                    <a href="{{ route('products') }}" target="_blank">
                        <div class="bg-gradient-base border border-blue-600 hover:bg-white  hover:text-blue-600 font-bold uppercase text-xs mt-5 py-2 px-4 rounded cursor-pointer">
                            Return
                        </div>
                    </a>
                </div>
        </div>
    </div>
</x-app-layout>
