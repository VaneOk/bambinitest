<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Checkout') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="leading-loose">

                    <form  method="post" class="max-w-xl m-4 p-10 bg-white rounded shadow-xl" action="{{route('checkout.order')}}" >
                        {!! csrf_field() !!}
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $message)
                                        <li>{{ $message }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <input style="display:none;" name="product_id" type="text" value="{{$product['id']}}">

                        <div class="border rounded-md max-w-md w-full px-4 py-3">
                            <div class="flex items-center justify-between">
                                <h3 class="text-gray-700 font-medium">Order total (1)</h3>
                            </div>
                            <div class="flex justify-between mt-6">
                                <div class="flex">
                                    <img class="h-20 w-20 object-cover rounded" src="https://images.unsplash.com/photo-1593642632823-8f785ba67e45?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=1189&amp;q=80" alt="">
                                    <div class="mx-3">
                                        <h3 class="text-sm text-gray-600">{{$product['name']}}</h3>
                                        <div class="flex items-center mt-2">
                                            <span class="text-gray-700 mx-2">1</span>
                                        </div>
                                    </div>
                                </div>
                                <span class="text-gray-600">{{$product['price']}}$</span>
                            </div>
                        </div>
                        <br>

                        <p class="text-gray-800 font-medium">Customer information</p>
                        <div class="">
                            <label class="block text-sm text-gray-00" for="cus_name">Name</label>
                            <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="cus_name" name="cus_name" type="text" required="" placeholder="Your Name" aria-label="Name">
                        </div>
                      {{--  <div class="mt-2">
                            <label class="block text-sm text-gray-600" for="cus_email">Email</label>
                            <input class="w-full px-5  py-4 text-gray-700 bg-gray-200 rounded" id="cus_email" name="cus_email" type="text" required="" placeholder="Your Email" aria-label="Email">
                        </div>--}}
                        <div class="mt-2">
                            <label class=" block text-sm text-gray-600" for="cus_email">Address</label>
                            <input class="w-full px-2 py-2 text-gray-700 bg-gray-200 rounded" id="cus_email" name="cus_address" type="text" required="" placeholder="Street" aria-label="Email">
                        </div>
                        <div class="mt-2">
                            <label class="hidden text-sm block text-gray-600" for="cus_email">City</label>
                            <input class="w-full px-2 py-2 text-gray-700 bg-gray-200 rounded" id="cus_email" name="cus_city" type="text" required="" placeholder="City" aria-label="Email">
                        </div>
                        <div class="inline-block mt-2 w-1/2 pr-1">
                            <label class="hidden block text-sm text-gray-600" for="cus_email">Country</label>
                            <input class="w-full px-2 py-2 text-gray-700 bg-gray-200 rounded" id="cus_email" name="cus_country" type="text" required="" placeholder="Country" aria-label="Email">
                        </div>

                        <br>
                        <br>
                        <div>
                            <h4 class="text-sm text-gray-500 font-medium">Delivery method</h4>
                            <div class="mt-6">
                                <button class="flex items-center justify-between w-full bg-white rounded-md border-2 border-blue-500 p-4 focus:outline-none">
                                    <label class="flex items-center">
                                        <input name="delivery_price" value="0" type="radio" class="form-radio h-5 w-5 text-blue-600" checked=""><span class="ml-2 text-sm text-gray-700">STANDARD</span>
                                    </label>
                                    <span class="text-gray-600 text-sm">$0</span>
                                </button>
                                <button class="mt-6 flex items-center justify-between w-full bg-white rounded-md border p-4 focus:outline-none">
                                    <label class="flex items-center">
                                        <input  name="delivery_price" value="10" type="radio" class="form-radio h-5 w-5 text-blue-600"><span class="ml-2 text-sm text-gray-700">EXPRESS</span>
                                    </label>
                                    <span class="text-gray-600 text-sm">$10</span>
                                </button>
                            </div>
                        </div>
                        <p class="mt-4 text-gray-800 font-medium">Payment information</p>
                        <div class="">
                            <label class="block text-sm text-gray-600" for="cus_name">Card</label>
                            <input class="w-full px-2 py-2 text-gray-700 bg-gray-200 rounded" id="cus_name" name="cus_card" type="text" required="" placeholder="Card Number MM/YY CVC" aria-label="Name">
                        </div>
                        <div class="mt-4">
                            <button class="px-4 py-1 text-white font-light tracking-wider bg-gray-900 rounded" type="submit">{{__('BUY NOW')}}</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</x-app-layout>
