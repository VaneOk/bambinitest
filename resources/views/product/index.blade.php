<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Products') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <table class="rounded-t-lg m-5 w-5/6 mx-auto bg-gray-200 text-gray-800">
                    <tr class="text-left border-b-2 border-gray-300">
                        <th class="px-4 py-3">{{__('Brand')}}</th>
                        <th class="px-4 py-3">{{__('Product')}}</th>
                        <th class="px-4 py-3">{{__('Price')}}</th>
                        <th class="px-4 py-3">{{__('Action')}}</th>
                    </tr>
                    @foreach($productProvider as $item)
                        <tr class="bg-gray-100 border-b border-gray-200">
                            <td class="px-4 py-3">{{$item->brand->name}}</td>
                            <td class="px-4 py-3">{{$item->name}}</td>
                            <td class="px-4 py-3">{{$item->price}}</td>
                            <td class="px-4 py-3">
                                <a class="inline-flex items-center h-8 px-4 m-2 text-sm text-indigo-100 transition-colors duration-150 bg-indigo-700 rounded-lg focus:shadow-outline hover:bg-indigo-800"
                                   href="{{route('checkout.checkout',['product_id' => $item->id])}}">
                                    BUY
                                </a>
                            </td>

                        </tr>
                    @endforeach

                </table>
                {{ $productProvider ? $productProvider->appends(request()->except('page'))->links() : '' }}

            </div>
        </div>
    </div>
</x-app-layout>
