<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Order</title>
</head>
<body>
<p>Order №{{$order_attributes['id']}}</p>
<p>Total Product Value - {{$order_attributes['total_product_value']}}</p>
<p>Total Shipping Value  -{{$order_attributes['total_shipping_value']}}</p>
<p>Client Name - {{$order_attributes['client_name']}}</p>
<p>Client Address - {{$order_attributes['client_address']}}</p>
<p>Created At - {{$order_attributes['created_at']}}</p>
<p>Product - №{{$product['id']}} Name{{$product['name']}} </p>
<p>Order Amount - {{$order_attributes['total_product_value'] + $order_attributes['total_shipping_value'] }}</p>
</body>
</html>
