<?php

use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\ProductsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::group([
    'middleware' => ['auth:sanctum', 'verified']
], function () {
    Route::get('/products', [ProductsController::class, 'actionIndex'])->name('products');
    Route::get('/checkout', [CheckoutController::class, 'actionCheckoutView'])->name('checkout.checkout');
    Route::post('/checkout', [CheckoutController::class, 'actionCreateOrder'])->name('checkout.order');
    Route::get('/success', [CheckoutController::class, 'actionSuccess'])->name('checkout.success');

});

