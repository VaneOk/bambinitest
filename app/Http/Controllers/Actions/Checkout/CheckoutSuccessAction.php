<?php

namespace App\Http\Controllers\Actions\Checkout;


use App\Http\Controllers\Actions\BaseAction;


class CheckoutSuccessAction extends BaseAction
{

    public $order_id;

    public function rules(): array
    {
        return [
            'order_id' => [
                'required',
                'integer'
            ],
        ];
    }

    public function action()
    {
        $this->view_data['order_id'] = $this->order_id;
    }

}
