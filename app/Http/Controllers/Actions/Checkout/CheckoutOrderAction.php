<?php

namespace App\Http\Controllers\Actions\Checkout;


use App\Actions\SubActions\Order\CreateOrder;
use App\Events\DeliveryOrderCreateEvent;
use App\Events\OrderCreateEvent;
use App\Http\Controllers\Actions\BaseAction;
use App\Repositories\ProductRepository;


class CheckoutOrderAction extends BaseAction
{

    public $product_id;
    public $cus_name;
    public $cus_address;
    public $cus_city;
    public $cus_country;
    public $delivery_price;
    public $cus_card;
    public $productRepository;

    public $shippingRepository;
    public $checkoutForm;

    public $resp_order_id;

    public function rules(): array
    {
        return [
            'product_id' => [
                'required',
                'integer'
            ],
            'cus_name' => [
                'required',
                'string',
            ],

            'cus_address' => [
                'required',
                'string',
            ],
            'cus_city' => [
                'required',
                'string',
            ],
            'cus_country' => [
                'required',
                'string',
            ],
            'delivery_price' => [
                'required',
                'string',
            ],
            'cus_card' => [
                'required',
                'string',
            ],
        ];
    }

    public function action()
    {
        $this->productRepository = new ProductRepository();

        $address = $this->cus_address . ', ' . $this->cus_city . ', ' . $this->cus_country;
        $product = $this->productRepository->findOrFail($this->product_id);

        $createAction = new CreateOrder([
            'product_id' => $this->product_id,
            'total_product_value' => $product->price,
            'total_shipping_value' => $this->delivery_price,
            'client_name' => $this->cus_name,
            'client_address' => $address,
        ]);

        $createAction->execute();
        event(new OrderCreateEvent($createAction->resp_order_attributes));

        $this->resp_order_id = $createAction->resp_order_attributes['id'];
    }

}
