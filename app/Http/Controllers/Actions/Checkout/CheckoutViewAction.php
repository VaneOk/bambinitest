<?php

namespace App\Http\Controllers\Actions\Checkout;


use App\Forms\Checkout\CheckoutForm;
use App\Http\Controllers\Actions\BaseAction;
use App\Repositories\ProductRepository;
use App\Repositories\ShippingRepository;

/**
 * Class CheckoutViewAction
 * @package App\Http\Controllers\Actions\Products
 * @property ProductRepository $productRepository
 * @property ShippingRepository $shippingRepository
 */
class CheckoutViewAction extends BaseAction
{

    public $product_id;


    public $productRepository;
    public $shippingRepository;
    public $checkoutForm;

    public function rules(): array
    {
        return [
            'product_id' => [
                'required',
                'integer'
            ]
        ];
    }

    public function action()
    {
        $this->productRepository = new ProductRepository();
        $this->shippingRepository = new ShippingRepository();
        $product = $this->productRepository->findOrFail($this->product_id);

        $this->checkoutForm = new CheckoutForm([
            'product_id' => $this->product_id,
            'product_value' => $product->price,
        ]);

        $this->view_data['form'] = $this->checkoutForm;
        $this->view_data['shippingList'] = $this->shippingRepository->getList();
        $this->view_data['product'] = $product->attributesToArray();

    }

}
