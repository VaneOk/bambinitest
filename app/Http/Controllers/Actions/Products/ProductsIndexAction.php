<?php

namespace App\Http\Controllers\Actions\Products;


use App\Http\Controllers\Actions\BaseAction;
use App\Repositories\ProductRepository;

class ProductsIndexAction extends BaseAction
{

    public $page;
    public $per_page;

    public $productRepository;

    public function rules(): array
    {
        return [
            'page' => ['integer', 'nullable'],
            'per_page' => ['integer', 'nullable'],
        ];
    }

    public function action()
    {
        $this->productRepository = new ProductRepository();

        $current_page = $this->page ?? self::DEFAULT_PAGE;
        $per_page = $this->per_page ?? self::DEFAULT_PER_PAGE;

        $this->view_data['productProvider'] = $this->productRepository->findWithPaginate(
            [],
            ['brand'],
            $per_page,
            ['*'],
            'page',
            $current_page
        );


    }

}
