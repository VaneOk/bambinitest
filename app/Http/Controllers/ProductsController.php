<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Actions\Products\ProductsIndexAction;
use App\Http\Requests\Products\ProductsIndexRequest;

class ProductsController extends Controller
{

    public function actionIndex(ProductsIndexRequest $request)
    {
        try {
            $action = new ProductsIndexAction(array_merge($request->all(), [

            ]));
            $action->execute();
            return view('product.index', $action->getViewData());
        } catch (\Exception $exception) {
            echo '<pre>';
            var_dump($exception->getMessage() . ' ' . $exception->getTraceAsString());
            echo '</pre>';
            die;
        }
    }
}
