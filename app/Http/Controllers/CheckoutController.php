<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Actions\Checkout\CheckoutOrderAction;
use App\Http\Controllers\Actions\Checkout\CheckoutSuccessAction;
use App\Http\Controllers\Actions\Checkout\CheckoutViewAction;
use App\Http\Requests\Checkout\CheckoutOrderRequest;
use App\Http\Requests\Checkout\CheckoutSuccessRequest;
use App\Http\Requests\Checkout\CheckoutViewRequest;

class CheckoutController extends Controller
{

    public function actionCheckoutView(CheckoutViewRequest $request)
    {
        try {
            $action = new CheckoutViewAction(array_merge($request->all(), [

            ]));
            $action->execute();
            return view('checkout.checkout', $action->getViewData());
        } catch (\Exception $exception) {
            echo '<pre>';
            var_dump($exception->getMessage() . ' ' . $exception->getTraceAsString());
            echo '</pre>';
            die;
        }
    }

    public function actionCreateOrder(CheckoutOrderRequest $request)
    {
        try {
            $action = new CheckoutOrderAction(array_merge($request->all(), [

            ]));
            $action->execute();
            return redirect()->route('checkout.success', ['order_id' => $action->resp_order_id]);
        } catch (\Exception $exception) {
            echo '<pre>';
            var_dump($exception->getMessage() . ' ' . $exception->getTraceAsString());
            echo '</pre>';
            die;
        }
    }

    public function actionSuccess(CheckoutSuccessRequest $request)
    {
        try {
            $action = new CheckoutSuccessAction(array_merge($request->all(), [

            ]));
            $action->execute();
            return view('checkout.success', $action->getViewData());
        } catch (\Exception $exception) {
            echo '<pre>';
            var_dump($exception->getMessage() . ' ' . $exception->getTraceAsString());
            echo '</pre>';
            die;
        }
    }


}
