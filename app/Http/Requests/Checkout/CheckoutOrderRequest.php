<?php

namespace App\Http\Requests\Checkout;

use App\Helper\ValidatorHelper;
use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;


class CheckoutOrderRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'product_id' => [
                'required',
                'integer',
                ValidatorHelper::createExists(Product::TABLE_NAME, Product::C_ID)
            ],
            'cus_name' => [
                'required',
                'string',
            ],
         /*   'cus_email' => [
                'required',
                'string',
            ],*/
            'cus_address' => [
                'required',
                'string',
            ],
            'cus_city' => [
                'required',
                'string',
            ],
            'cus_country' => [
                'required',
                'string',
            ],
        /*    'cus_zip' => [
                'required',
                'string',
            ],*/
            'delivery_price' => [
                'required',
                'string',
            ],
            'cus_card' => [
                'required',
                'string',
            ],

        ];
    }


}
