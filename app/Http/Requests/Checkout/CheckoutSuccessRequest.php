<?php

namespace App\Http\Requests\Checkout;

use App\Helper\ValidatorHelper;
use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;


class CheckoutSuccessRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'order_id' => [
                'required',
                'integer',
                ValidatorHelper::createExists(Product::TABLE_NAME, Product::C_ID)
            ],
        ];
    }


}
