<?php

namespace App\Http\Requests\Products;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ProductsIndexRequest
 * @package App\Http\Requests\Product
 */
class ProductsIndexRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'page' => ['integer', 'nullable'],
        ];
    }


}
