<?php

namespace App\Models;

use App\Models\Interfaces\OrderInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model implements OrderInterface
{
    use HasFactory;
}
