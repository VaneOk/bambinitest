<?php

namespace App\Models;

use App\Models\Interfaces\ProductInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model implements ProductInterface
{
    use HasFactory;

    protected $fillable = [
        'name', 'brand_id', 'price'
    ];

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }
}
