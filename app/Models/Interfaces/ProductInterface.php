<?php

namespace App\Models\Interfaces;

interface ProductInterface
{
    const C_ID = 'id';
    const C_BRAND_ID = 'brand_id';
    const C_NAME = 'name';
    const C_PRICE = 'price';
    const C_UPDATED_AT = 'updated_at';
    const C_CREATED_AT = 'created_at';

    const TABLE_NAME = 'products';
}
