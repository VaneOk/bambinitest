<?php

namespace App\Models\Interfaces;

interface OrderInterface
{
    const C_ID = 'id';
    const C_TOTAL_PRODUCT_VALUE = 'total_product_value';
    const C_TOTAL_SHIPPING_VALUE = 'total_shipping_value';
    const C_CLIENT_NAME = 'client_name';
    const C_CLIENT_ADDRESS = 'client_address';
    const C_CREATED_AT = 'created_at';
    const C_UPDATED_AT = 'updated_at';
    const C_PRODUCT_ID = 'product_id';

    const TABLE_NAME = 'orders';
}
