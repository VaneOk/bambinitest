<?php

namespace App\Models\Interfaces;

interface BrandInterface
{
    const C_ID = 'id';
    const C_NAME = 'name';
    const C_CREATED_AT = 'created_at';
    const C_UPDATED_AT = 'update_at';

    const TABLE_NAME = 'brands';
}
