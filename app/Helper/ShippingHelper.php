<?php

namespace App\Helper;


use App\Forms\Shipping\ShippingForm;
use App\Forms\Shipping\ShippingPriceForm;

class ShippingHelper
{
    const TYPE_SHIPPING_STANDARD_ID = 1;
    const TYPE_SHIPPING_STANDARD_KEY = 'standard';
    const TYPE_SHIPPING_STANDARD_PRICE = 0;

    const TYPE_SHIPPING_EXPRESS_ID = 2;
    const TYPE_SHIPPING_EXPRESS_KEY = 'express';
    const TYPE_SHIPPING_EXPRESS_PRICE = 10;


    public static function getShippingList()
    {
        return [
            self::TYPE_SHIPPING_STANDARD_ID => [
                'key' => self:: TYPE_SHIPPING_STANDARD_KEY,
                'title' => 'STANDARD',
                'price' => self::TYPE_SHIPPING_STANDARD_PRICE
            ],
            self::TYPE_SHIPPING_EXPRESS_ID => [
                'key' => self::TYPE_SHIPPING_EXPRESS_KEY,
                'title' => 'EXPRESS',
                'price' => self::TYPE_SHIPPING_EXPRESS_PRICE
            ],
        ];
    }

    public static function getShippingTypeByID($id)
    {
        return isset(self::getShippingList()[$id]) ? self::getShippingList()[$id] : '';
    }


    public static function getShippingPrice(ShippingForm $shippingForm): ShippingPriceForm
    {

        if ($shippingForm->shipping_type_id == self::TYPE_SHIPPING_STANDARD_ID) {
            return new ShippingPriceForm([
                'price' => 0
            ]);
        } elseif ($shippingForm->shipping_type_id == self::TYPE_SHIPPING_EXPRESS_ID) {
            return new ShippingPriceForm([
                'price' => 10
            ]);
        } else {
            throw new \Exception('Unknown Shipping Type');
        }

    }

}
