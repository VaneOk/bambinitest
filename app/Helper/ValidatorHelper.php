<?php

namespace App\Helper;


class ValidatorHelper
{
    public static function createExists($table, $column, $connection = '')
    {
        $str = 'exists:';
        if ($connection) {
            $str .= $connection . '.';
        }
        $str .= $table . ',' . $column;
        return $str;
    }
}
