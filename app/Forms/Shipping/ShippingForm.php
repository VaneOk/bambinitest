<?php

namespace App\Forms\Shipping;


use App\Forms\BaseForm;

class ShippingForm extends BaseForm
{
    public $product_id;
    public $shipping_type_id;
    public $address;

    public function rules(): array
    {
        return [
            'product_id' => ['reqired'],
            'shipping_type_id' => ['reqired'],
            'address' => ['reqired'],
        ];
    }
}
