<?php

namespace App\Forms\Shipping;

use App\Forms\BaseForm;

class ShippingPriceForm extends BaseForm
{
    public $price;

    public function rules(): array
    {
        return [
            'price' => ['reqired'],
        ];
    }
}
