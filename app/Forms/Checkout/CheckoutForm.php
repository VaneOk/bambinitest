<?php

namespace App\Forms\Checkout;


use App\Forms\BaseForm;

class CheckoutForm extends BaseForm
{
    public $product_id;
    public $client_name;
    public $client_address;
    public $shipping_type_id;

    public function rules(): array
    {
        return [
            'product_id' => ['reqired'],
            'client_name' => ['reqired'],
            'client_address' => ['reqired'],
            'shipping_type_id' => ['reqired'],
        ];
    }
}
