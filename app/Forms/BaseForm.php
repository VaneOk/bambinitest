<?php

namespace App\Forms;


use Illuminate\Support\Facades\Validator;

abstract class BaseForm
{
    protected $validator;


    public function rules(): array
    {
        return [];
    }

    public function __construct($data)
    {
        $class_vars = get_object_vars($this);
        foreach ($class_vars as $key => $var) {
            if (isset($data[$key])) {
                $this->$key = $data[$key];
            }
        }
        $class_vars = get_object_vars($this);
        $this->validator = Validator::make($class_vars, $this->rules());
    }

    public function validate()
    {
        if ($this->validator->fails()) {
            return $this->validator->errors();
        }
        return null;
    }

}
