<?php

namespace App\Repositories\Interfaces;

use App\Models\Product;

interface ProductRepositoryInterface
{

    public function findOrFail($id);

    public function findWithPaginate($condition = [], $with = [], $perPage = null, $columns = ['*'], $pageName = 'page', $page = null);
}
