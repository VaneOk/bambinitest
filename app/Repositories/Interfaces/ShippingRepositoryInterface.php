<?php

namespace App\Repositories\Interfaces;

use App\Models\Product;

interface ShippingRepositoryInterface
{
    public function getList();
}
