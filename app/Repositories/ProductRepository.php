<?php

namespace App\Repositories;

use App\Models\Product;
use App\Repositories\Interfaces\ProductRepositoryInterface;

class ProductRepository implements ProductRepositoryInterface
{


    public function findOrFail($id)
    {
        return Product::query()->findOrFail($id);
    }

    public function findWithPaginate($condition = [], $with = [], $perPage = null, $columns = ['*'], $pageName = 'page', $page = null)
    {
        return Product::query()->where($condition)->with($with)->paginate($perPage, $columns, $pageName, $page);
    }
}
