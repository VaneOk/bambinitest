<?php

namespace App\Repositories;

use App\Helper\ShippingHelper;
use App\Repositories\Interfaces\ShippingRepositoryInterface;

class ShippingRepository implements ShippingRepositoryInterface
{
    public function getList()
    {
        return ShippingHelper::getShippingList();
    }
}
