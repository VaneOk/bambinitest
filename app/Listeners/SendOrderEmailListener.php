<?php

namespace App\Listeners;

use App\Actions\SubActions\Order\SendOrderEmailToAdmin;
use App\Events\OrderCreateEvent;


class SendOrderEmailListener
{

    public function handle(OrderCreateEvent $event)
    {
        $sendToAdminAction = new SendOrderEmailToAdmin([
            'order_attributes' => $event->order
        ]);
        $sendToAdminAction->execute();

    }
}
