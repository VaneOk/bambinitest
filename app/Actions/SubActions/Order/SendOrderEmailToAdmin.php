<?php

namespace App\Actions\SubActions\Order;


use App\Http\Controllers\Actions\BaseAction;
use App\Models\Product;
use App\Notifications\Order\OrderCreateNotification;
use Illuminate\Support\Facades\Notification;


class SendOrderEmailToAdmin extends BaseAction
{

    public $order_attributes;

    public function rules(): array
    {
        return [
            'order_attributes' => [
                'required',
            ]
        ];
    }

    public function action()
    {
        $admin_email = config('app.admin_email');
        $product = Product::query()->findOrFail($this->order_attributes['product_id'])->attributesToArray();
        try {
            Notification::route('mail', $admin_email)
                ->notify(new OrderCreateNotification([
                    'order_attributes' => $this->order_attributes,
                    'subject' => 'BambiniTest Order #' . $this->order_attributes['id'],
                    'product' => $product
                ]));
        } catch (\Exception $exception) {
            // Save logs
        }

    }

}
