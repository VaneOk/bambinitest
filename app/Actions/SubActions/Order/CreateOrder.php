<?php

namespace App\Actions\SubActions\Order;


use App\Http\Controllers\Actions\BaseAction;
use App\Models\Order;


class CreateOrder extends BaseAction
{

    public $product_id;
    public $total_product_value;
    public $total_shipping_value;
    public $client_name;
    public $client_address;


    public $resp_order_attributes;

    public function rules(): array
    {
        return [
            'product_id' => [
                'required',
                'integer'
            ],
            'total_product_value' => [
                'required',
                'string',
            ],
            'total_shipping_value' => [
                'required',
                'string',
            ],
            'client_name' => [
                'required',
                'string',
            ],
            'client_address' => [
                'required',
                'string',
            ],
        ];
    }

    public function action()
    {
        $order = new Order();
        $order->product_id = $this->product_id;
        $order->total_product_value = $this->total_product_value;
        $order->total_shipping_value = $this->total_shipping_value;
        $order->client_name = $this->client_name;
        $order->client_address = $this->client_address;
        if (!$order->save()) {
            throw new \Exception('CreateOrder Error');
        }
        $this->resp_order_attributes = $order->attributesToArray();
    }

}
