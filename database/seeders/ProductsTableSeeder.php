<?php

namespace Database\Seeders;

use App\Models\Brand;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Brand::factory()->count(5)->hasProducts(5)->create();

        /*(new BrandFactory(5))->create()->each(
            function ($brand) {
                $brand->products()->saveMany(
                    (new ProductFactory())->count(5)->make()
                );
            });*/
    }
}
