<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    private $table = 'products';
    private $brand_table = 'brands';

    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('brand_id')->nullable(true);
            $table->string('name', 255);
            $table->decimal('price', 8, 2);
            $table->dateTime('created_at');
            $table->dateTime('updated_at');

            $table->foreign(['brand_id'])->on($this->brand_table)->references('id');
        });
    }

    public function down()
    {
        Schema::table($this->table, function (Blueprint $table) {
            $table->dropForeign(['brand_id']);
        });
        Schema::dropIfExists($this->table);
    }
}
