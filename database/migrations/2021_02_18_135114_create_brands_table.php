<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBrandsTable extends Migration
{

    private $table = 'brands';

    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }


    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
