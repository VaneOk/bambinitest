<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    private $table = 'orders';
    private $product_table = 'products';

    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->id();
            $table->decimal('total_product_value', 8, 2);
            $table->decimal('total_shipping_value', 8, 2);
            $table->string('client_name', 255);
            $table->string('client_address', 255);
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
            $table->unsignedBigInteger('product_id');

            $table->foreign(['product_id'])->on($this->product_table)->references('id');
        });
    }

    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
